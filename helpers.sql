CREATE OR REPLACE FUNCTION public.mcoalesce(
	ids anyelement)
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
    
AS $BODY$
begin
	
	return case when ids is null then '' else ids::varchar end;

END;
$BODY$;