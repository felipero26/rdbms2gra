with constrains as (
	SELECT	tc.constraint_name, 
			tc.table_name, 
			kcu.column_name, 
			ccu.table_name AS foreign_table_name,
			ccu.column_name AS foreign_column_name 
	FROM	information_schema.table_constraints AS tc 
	JOIN 	information_schema.key_column_usage AS kcu
			ON tc.constraint_name = kcu.constraint_name
			AND tc.table_schema = kcu.table_schema
	JOIN 	information_schema.constraint_column_usage AS ccu
		  ON ccu.constraint_name = tc.constraint_name
		  AND ccu.table_schema = tc.table_schema
	WHERE tc.constraint_type = 'FOREIGN KEY'
)

select	'@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .'
union all
select	'@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .'
union all
select	'@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .'

union all

select	distinct '@prefix table_' || table_name || ': <http://mihotel.com/hotel/schemas/' || table_name || '#> .'
FROM	INFORMATION_SCHEMA.COLUMNS
where	table_schema = 'public'

union all

SELECT	distinct 'table_' || table_name || ':' || table_name || ' a rdfs:Class ; rdfs:label "' || table_name || '" ; rdfs:comment "table ' || table_name || '" .'
FROM	INFORMATION_SCHEMA.COLUMNS
where	table_schema = 'public'

union all

SELECT	'table_' || gen.table_name || ':' || gen.column_name || ' a rdf:Property ;'
		|| ' rdfs:domain table_' || gen.table_name || ':' || gen.table_name || ' ;'
		|| case when constraint_name is null then ' rdfs:range xsd:string ;' else 'rdfs:range table_' || foreign_table_name || ':' || foreign_table_name || ' ;' end
		|| ' rdfs:label "' || gen.table_name || ' ' || gen.column_name || '" .'
FROM	INFORMATION_SCHEMA.COLUMNS as gen
left	join	constrains on constrains.column_name = gen.column_name and constrains.table_name = gen.table_name
where	table_schema = 'public'