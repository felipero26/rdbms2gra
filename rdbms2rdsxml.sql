CREATE OR REPLACE FUNCTION public.rdbms2rdsxml()
    RETURNS text
    LANGUAGE 'plpgsql'

    COST 100
    VOLATILE 
AS $BODY$

declare
	cad varchar;
	aux varchar;
	r	record;
	auxs varchar;
	auxr varchar;
	x 	varchar;
	tbpk varchar;
	isfk varchar;
	tbfk varchar;
begin

	cad =  '<?xml version="1.0"?> <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" '; 

	select	string_agg(d, ' ') into aux
	from(
		select	'xmlns:' || table_name || '="http://mihotel.com/hotel/schemas/' || table_name || '#"' as d
		FROM	INFORMATION_SCHEMA.COLUMNS
		where	table_schema = 'public'
		group	by table_name
	) as t1;

	cad = cad || aux;
	
	cad = cad || '>';
	
	FOR r IN
        select	table_name, array_agg(column_name) as columns
		FROM	INFORMATION_SCHEMA.COLUMNS
		where	table_schema = 'public'
		group	by table_name
		order	by table_name
    LOOP
        
		SELECT	kcu.column_name into tbpk
		FROM 	information_schema.table_constraints AS tc 
		JOIN 	information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
			  	AND tc.table_schema = kcu.table_schema
		WHERE 	tc.constraint_type = 'PRIMARY KEY' AND tc.table_name = r.table_name;
		
		SELECT	ccu.column_name into isfk
		FROM 	information_schema.table_constraints AS tc 
		JOIN 	information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
				AND tc.table_schema = kcu.table_schema
		JOIN 	information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name
				AND ccu.table_schema = tc.table_schema
		WHERE 	tc.constraint_type = 'FOREIGN KEY' AND ccu.table_name = r.table_name;
		
		if isfk is null then
			auxs = 'select string_agg(va, '' '') from ( select ''<'|| r.table_name ||' rdf:about="http://mihotel.com/hotel/schemas/'|| r.table_name ||'#''|| ROW_NUMBER() over() ||''">''';
		else
			auxs = 'select string_agg(va, '' '') from ( select ''<'|| r.table_name ||' rdf:about="http://mihotel.com/hotel/schemas/'|| r.table_name ||'#''|| ' || tbpk  || ' ||''">''';
		end if;
		
		FOREACH x in ARRAY r.columns loop
			
			SELECT	ccu.table_name into tbfk
			FROM 	information_schema.table_constraints AS tc 
			JOIN 	information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name
					AND tc.table_schema = kcu.table_schema
			JOIN 	information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name
					AND ccu.table_schema = tc.table_schema
			WHERE 	tc.constraint_type = 'FOREIGN KEY' AND tc.table_name = r.table_name and kcu.column_name = x;
			
			if tbfk is null then
				auxs = auxs || '||''<'|| r.table_name ||':'|| x ||'>''|| mcoalesce('|| x ||') ||''</'|| r.table_name ||':'|| x ||'>''';
			else
				auxs = auxs || '||''<'|| r.table_name ||':'|| x ||'><' || tbfk || ' rdf:about="http://mihotel.com/hotel/schemas/'|| tbfk ||'#''|| mcoalesce('|| x ||') ||''"/></'|| r.table_name ||':'|| x ||'>''';
			end if;
		end loop;
		
		auxs = auxs || '||''</' || r.table_name || '>'' as va from ' || r.table_name || ' ) as t1';
		
		execute auxs into auxr;
		
		cad = cad || auxr;
		
    END LOOP;

	cad = cad || '</rdf:RDF>';
	
	cad = REGEXP_REPLACE(cad, '<[a-zA-Z0-9]+ rdf:about="[a-zA-Z0-9\:\/\.]+#"/>', '', 'g');
	cad = REGEXP_REPLACE(cad, '<[a-zA-Z0-9]+:[a-zA-Z0-9]+>\s*</[a-zA-Z0-9]+:[a-zA-Z0-9]+>', '', 'g');
	
	return cad;

end
$BODY$;

--test 
--select * from rdbms2rdsxml();
